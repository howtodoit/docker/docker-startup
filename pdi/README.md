# Esempio di utilizzo di Pentaho Data Integration

Pull di una ummagine con Pentaho Data Integration (pdi-ce-7.1.0.0-12) installata

```bash
docker pull mvnfenzan/pentaho-di
```

Esecuzione del Job SampleJob.kjb

```bash
docker run --rm -ti -v ${PWD}:/home/pentaho/repo mvnfenzan/pentaho-di -file /home/pentaho/repo/SampleJob.kjb
```

Lo script visualizza semplicemnte la scritta di log **hello, world**

L'output atteso è del tipo:

```bash
Illegal option -p
#######################################################################
WARNING:  no libwebkitgtk-1.0 detected, some features will be unavailable
    Consider installing the package with apt-get or yum.
    e.g. 'sudo apt-get install libwebkitgtk-1.0-0'
#######################################################################
OpenJDK 64-Bit Server VM warning: ignoring option MaxPermSize=256m; support was removed in 8.0
^C[spadaro@spadaro-fedora pdi]$ docker run --rm -ti -v ${PWD}:/home/pentaho/repovnfenzan/pentaho-di -file /home/pentaho/repo/SampleJob.kjb
Illegal option -p
#######################################################################
WARNING:  no libwebkitgtk-1.0 detected, some features will be unavailable
    Consider installing the package with apt-get or yum.
    e.g. 'sudo apt-get install libwebkitgtk-1.0-0'
#######################################################################
OpenJDK 64-Bit Server VM warning: ignoring option MaxPermSize=256m; support was removed in 8.0
16:53:33,572 INFO  [KarafBoot] Checking to see if org.pentaho.clean.karaf.cache is enabled
16:53:33,696 INFO  [KarafInstance] 
*******************************************************************************
*** Karaf Instance Number: 1 at /usr/local/lib/data-integration/./system/ka ***
***   raf/caches/kitchen/data-1                                             ***
*** FastBin Provider Port:52901                                             ***
*** Karaf Port:8802                                                         ***
*** OSGI Service Port:9051                                                  ***
*******************************************************************************
Jan 18, 2019 4:53:34 PM org.apache.karaf.main.Main$KarafLockCallback lockAquired
INFO: Lock acquired. Setting startlevel to 100
2019/01/18 16:53:34 - Kitchen - Start of run.
2019/01/18 16:53:34 - RepositoriesMeta - Reading repositories XML file: /home/pentaho/.kettle/repositories.xml
2019-01-18 16:53:39.094:INFO:oejs.Server:jetty-8.1.15.v20140411
2019-01-18 16:53:39.207:INFO:oejs.AbstractConnector:Started NIOSocketConnectorWrapper@0.0.0.0:9051
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/core
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/configuration/beans
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/configuration/parameterized-types
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/configuration/security
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://schemas.xmlsoap.org/wsdl/
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://www.w3.org/2005/08/addressing
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://schemas.xmlsoap.org/ws/2004/08/addressing
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-management (152) [org.apache.cxf.management.InstrumentationManager]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-wsdl (189) [org.apache.cxf.wsdl.WSDLManager]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-transports-http (156) [org.apache.cxf.transport.http.HTTPTransportFactory, org.apache.cxf.transport.http.HTTPWSDLExtensionLoader, org.apache.cxf.transport.http.policy.HTTPClientAssertionBuilder, org.apache.cxf.transport.http.policy.HTTPServerAssertionBuilder, org.apache.cxf.transport.http.policy.NoOpPolicyInterceptorProvider]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-ws-policy (203) [org.apache.cxf.ws.policy.PolicyEngine, org.apache.cxf.policy.PolicyDataEngine, org.apache.cxf.ws.policy.AssertionBuilderRegistry, org.apache.cxf.ws.policy.PolicyInterceptorProviderRegistry, org.apache.cxf.ws.policy.PolicyBuilder, org.apache.cxf.ws.policy.PolicyAnnotationListener, org.apache.cxf.ws.policy.attachment.ServiceModelPolicyProvider, org.apache.cxf.ws.policy.attachment.external.DomainExpressionBuilderRegistry, org.apache.cxf.ws.policy.attachment.external.EndpointReferenceDomainExpressionBuilder, org.apache.cxf.ws.policy.attachment.external.URIDomainExpressionBuilder, org.apache.cxf.ws.policy.attachment.wsdl11.Wsdl11AttachmentPolicyProvider, org.apache.cxf.ws.policy.mtom.MTOMAssertionBuilder, org.apache.cxf.ws.policy.mtom.MTOMPolicyInterceptorProvider]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/transports/http/configuration
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-bindings-soap (192) [org.apache.cxf.binding.soap.SoapBindingFactory, org.apache.cxf.binding.soap.SoapTransportFactory]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/jaxrs
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/jaxrs-client
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-bindings-xml (191) [org.apache.cxf.binding.xml.XMLBindingFactory, org.apache.cxf.binding.xml.wsdl11.XMLWSDLExtensionLoader]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/bindings/soap
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/simple
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-frontend-jaxws (194) [org.apache.cxf.jaxws.context.WebServiceContextResourceResolver]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/jaxws
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/binding/coloc
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-transports-local (199) [org.apache.cxf.transport.local.LocalTransportFactory]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-bindings-object (200) [org.apache.cxf.binding.object.ObjectBindingFactory]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/blueprint/binding/object
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/policy
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://www.w3.org/ns/ws-policy
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://www.w3.org/2006/07/ws-policy
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://schemas.xmlsoap.org/ws/2004/09/policy
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://www.w3.org/2000/09/xmldsig#
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://docs.oasis-open.org/ws-sx/ws-securitypolicy/200702
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-ws-addr (220) [org.apache.cxf.ws.addressing.policy.AddressingAssertionBuilder, org.apache.cxf.ws.addressing.policy.UsingAddressingAssertionBuilder, org.apache.cxf.ws.addressing.policy.AddressingPolicyInterceptorProvider, org.apache.cxf.ws.addressing.impl.AddressingWSDLExtensionLoader, org.apache.cxf.ws.addressing.WSAddressingFeature$WSAddressingFeatureApplier, org.apache.cxf.ws.addressing.MAPAggregator$MAPAggregatorLoader]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/ws/addressing
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-ws-security (222) [org.apache.cxf.ws.security.policy.WSSecurityPolicyLoader, org.apache.cxf.ws.security.cache.CacheCleanupListener]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-ws-rm (224) [org.apache.cxf.ws.rm.RMManager, org.apache.cxf.ws.rm.policy.RMPolicyInterceptorProvider, org.apache.cxf.ws.rm.policy.RM10AssertionBuilder, org.apache.cxf.ws.rm.policy.RM12AssertionBuilder, org.apache.cxf.ws.rm.policy.WSRMP12PolicyLoader, org.apache.cxf.ws.rm.policy.MC11PolicyLoader, org.apache.cxf.ws.rm.policy.RSPPolicyLoader]
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://cxf.apache.org/ws/rm/manager
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.blueprint.NamespaceHandlerRegisterer register
INFO: Registered blueprint namespace handler for http://schemas.xmlsoap.org/ws/2005/02/rm/policy
Jan 18, 2019 4:53:40 PM org.apache.cxf.bus.osgi.CXFExtensionBundleListener addExtensions
INFO: Adding the extensions from bundle org.apache.cxf.cxf-rt-javascript (225) [org.apache.cxf.javascript.JavascriptServerListener]
Jan 18, 2019 4:53:40 PM org.pentaho.caching.impl.PentahoCacheManagerFactory$RegistrationHandler$1 onSuccess
INFO: New Caching Service registered
2019/01/18 16:53:44 - SampleJob - Start of job execution
2019/01/18 16:53:44 - SampleJob - Starting entry [Shell]
2019/01/18 16:53:44 - Shell - Running on platform : Linux
2019/01/18 16:53:44 - Shell - Executing command : /tmp/kettle_9db8aa14-1b41-11e9-8f3e-d7809edce986shell
2019/01/18 16:53:44 - Shell - (stdout) hello, world
2019/01/18 16:53:44 - SampleJob - Starting entry [Successo]
2019/01/18 16:53:44 - SampleJob - Finished job entry [Successo] (result=[true])

```